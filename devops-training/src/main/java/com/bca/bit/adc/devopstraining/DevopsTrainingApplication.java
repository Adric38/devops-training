package com.bca.bit.adc.devopstraining;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevopsTrainingApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevopsTrainingApplication.class, args);
		System.out.println("Hello World!/n" + "My name is Adric Hungriko, nice to meet you!"
	}

}
